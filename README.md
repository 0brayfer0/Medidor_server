Medidor de contenido frontend

1. Realiza extración del texto de un PDF usando OCR tesseract
2. Identifica si hay plagio o no en oraciones.
3. Puntaje = (Analizador léxico + numeroPalabras)/(1 + puntajeLider)

Descarga del modelo word2vec
http://cs.famaf.unc.edu.ar/~ccardellino/SBWCE/SBW-vectors-300-min5.bin.gz