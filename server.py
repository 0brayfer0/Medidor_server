from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy

import os

dbdir = "sqlite:///" + os.path.abspath(os.getcwd()) + "/medidor.db"

import shlex,subprocess
def ExtraerTexto(nombrePDF):
    #Comando para poder convertir el PDF a imagen
    command_line='convert -density 300 ' + nombrePDF + ' -depth 8 -strip -background white -alpha off salida.tiff'
    args=shlex.split(command_line)
    subprocess.call(args)
    
    #Comando para poder extraer el texto de la imagen
    command='tesseract salida.tiff output'
    argument=shlex.split(command)
    subprocess.call(argument)
    archivo=open('output.txt','r')
    
    #Almacenamos el texto de la imagen en la variable cad
    cad=archivo.read()
    archivo.close()
    return cad



# funciones extras
from googletrans import Translator
import nltk
from nltk.stem import PorterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.tokenize import PunktSentenceTokenizer
from nltk.corpus import stopwords, wordnet, state_union
import threading
import time
import datetime
import logging
from multiprocessing.pool import ThreadPool

def Translate(palabra):
    translator = Translator()
    a = translator.translate(palabra)
    word = a.text
    return word
        
def Traducir(word):
    translator = Translator()
    a = translator.translate(word,dest = 'es')
    palabra = a.text
    return palabra
    
def TranslateToEnglish(palabra):
    translator = Translator()
    texto = nltk.sent_tokenize(palabra)
    traducir = ''
    for i in range(0, len(texto)):
        traducir = traducir + ' '  + Translate(texto[i])
    return traducir

def TraducirAEspanol(palabra):
    translator = Translator()
    texto = nltk.sent_tokenize(palabra)
    traducir = texto[0]
    for i in range(1, len(texto)):
        traducir = traducir + '. ' + Traducir(texto[i])
    return traducir

def eliminaImperfecciones(palabra):
    a = palabra.replace('to ', '')
    b = a.replace('i will ', '')
    c = b.replace('I will', '')
    d = c.replace('i ', '')
    e = d.replace('I ', '')
    f = e.replace('will ', '')
    return f


def infinitivo(word):
    ps = PorterStemmer()
    return  eliminaImperfecciones(ps.stem(word))



def eliminaGuion(lista):
    for i in range(0, len(lista)):
        lista[i] = lista[i].replace('_', ' ')
    return lista

def comparaPalabraEnLista(palabra, lista):
    cont = 0
    for i in range(0, len(lista)):
        if( palabra == lista[i]):
            cont = cont + 1
    if(cont >= 1):
        return lista
    else:
        return lista + [palabra]
    
    
def sinonimos(palabra):
    #Traducimos al ingles con la función creada
    word = TranslateToEnglish(palabra)
    word = eliminaImperfecciones(word)
    
    #Creamos una lista vacía para guardar los sinónimos
    synonyms = []
    
    #Llenamos la lista con los sinonimos con la palabra dada
    for syn in wordnet.synsets(word):
        for l in syn.lemmas():
            synonyms.append(l.name())
    
    #Convertimos la variable synonyms a tipo lista sin repeticiones
    synonyms = list(set(synonyms))
    synonyms = eliminaGuion(synonyms)
    synonyms = comparaPalabraEnLista(palabra, synonyms)
    
    for i in range(0, len(synonyms)):
        synonyms[i] = eliminaImperfecciones(synonyms[i])
    
    synonyms1 = convierteInfinitivoLista(synonyms)
    
    if synonyms == []:
        return [palabra]
    else:
        return list(set(synonyms + synonyms1))
    
    
def identificaSinonimo(palabra1, palabra2):
    cont = 0
    listaSinonimos = sinonimos(palabra1)
    for i in range(0, len(listaSinonimos)):
        if( palabra2 == listaSinonimos[i]):
            cont = cont + 1
    if( cont >= 1 ):
        return True
    else:
        return False    
    
    
def convierteInfinitivoLista(lista):
    conjunto = set(lista)
    texto = list(conjunto)
    
    for i in range(0, len(texto)):
        texto[i] = infinitivo(texto[i])
        
    return texto 
    
    
def similitud(word1, word2):
    word1 = infinitivo(word1)
    word2 = infinitivo(word2)
    return identificaSinonimo(word1, word2)  
    
    
    
def similitud1(word1, word2):
    pool1 = ThreadPool(processes=1)
    pool2 = ThreadPool(processes=1)
    
    async_result1 = pool1.apply_async(infinitivo, (word1,))
    async_result2 = pool2.apply_async(infinitivo, (word2,))
    
    word1  = async_result1.get()
    word2 = async_result2.get()
    
    return identificaSinonimo(word1, word2)
    
    
def separaPalabras(texto):
    Texto = TranslateToEnglish(texto)
    return nltk.word_tokenize(Texto)
    
    
def elementosOracion(oracion):
    Oracion = separaPalabras(oracion)
    #nltk.corpus.treebank.tagged_words(tagset='universal')
    return nltk.pos_tag(Oracion, tagset='universal')  
    
    
def verbo(oracion):
    Oracion = elementosOracion(oracion)
    verbos = []
    for i in range(0, len(Oracion)):
        if( Oracion[i][1] == 'VERB' and Oracion[i][0] != 'will'):
            verbos.append(Oracion[i][0])
    return verbos        



def sustantivo(oracion):
    Oracion = elementosOracion(oracion)
    sustantivos = []
    for i in range(0, len(Oracion)):
        if( Oracion[i][1] == 'NOUN'):
            sustantivos.append(Oracion[i][0])
    return sustantivos




def adjetivo(oracion):
    Oracion = elementosOracion(oracion)
    adjetivos = []
    for i in range(0, len(Oracion)):
        if( Oracion[i][1] == 'ADJ'):
            adjetivos.append(Oracion[i][0])
    return adjetivos




def adverbio(oracion):
    Oracion = elementosOracion(oracion)
    adverbios = []
    for i in range(0, len(Oracion)):
        if( Oracion[i][1] == 'ADV'):
            adverbios.append(Oracion[i][0])
    return adverbios



def otro(oracion):
    Oracion = elementosOracion(oracion)
    otros = []
    for i in range(0, len(Oracion)):
        if( Oracion[i][1] == 'X'):
            otros.append(Oracion[i][0])
    return otros




def separaOraciones(texto):
    Texto = TranslateToEnglish(texto)
    return nltk.sent_tokenize(Texto)



def separaOraciones(texto):
    Texto = TranslateToEnglish(texto)
    return nltk.sent_tokenize(Texto)

def verbosOraciones(parrafo):
    verbos = []
    Oraciones = separaOraciones(parrafo)
    for oracion in Oraciones:
        verbos = verbos + verbo(oracion)
    return verbos        


def sustantivosOraciones(parrafo):
    sustantivos = []
    Oraciones = separaOraciones(parrafo)
    for oracion in Oraciones:
        sustantivos = sustantivos + sustantivo(oracion)
    return sustantivos 


def adjetivosOraciones(parrafo):
    adjetivos = []
    Oraciones = separaOraciones(parrafo)
    for oracion in Oraciones:
        adjetivos = adjetivos + adjetivo(oracion)
    return adjetivos



def adverbioOraciones(parrafo):
    adverbios = []
    Oraciones = separaOraciones(parrafo)
    for oracion in Oraciones:
        adverbios = adverbios + adverbio(oracion)
    return adverbios



def otrosOraciones(parrafo):
    otros = []
    Oraciones = separaOraciones(parrafo)
    for oracion in Oraciones:
        otros = otros + otro(oracion)
    return otros


def otrosOraciones(parrafo):
    otros = []
    Oraciones = separaOraciones(parrafo)
    for oracion in Oraciones:
        otros = otros + otro(oracion)
    return otros

def similitudVerbosPorcentaje1(texto1, texto2):
    pool1 = ThreadPool(processes=1)
    pool2 = ThreadPool(processes=1)
    
    async_result1 = pool1.apply_async(verbosOraciones, (texto1,))
    async_result2 = pool2.apply_async(verbosOraciones, (texto2,))
    
    #verbos1 = verbosOraciones(texto1)
    #verbos2 = verbosOraciones(texto2)
    
    verbos1 = async_result1.get()
    verbos2 = async_result2.get()
    
    pool3 = ThreadPool(processes=1)
    pool4 = ThreadPool(processes=1)
    
    async_result3 = pool3.apply_async(convierteInfinitivoLista, (verbos1,))
    async_result4 = pool4.apply_async(convierteInfinitivoLista, (verbos2,))

    Verbos1 = async_result3.get()
    Verbos2 = async_result4.get()


    tamanioUnion = len(Verbos1) + len(Verbos2)
    cont1 = 0

    for i in range(0, len(Verbos1)):
        for j in range(0, len(Verbos2)):
            #print('se compara {0} con {1}'.format(Verbos1[i], Verbos2[j]))
            #print(identificaSinonimo(Verbos1[i], Verbos2[j]))
            if( identificaSinonimo(Verbos1[i],Verbos2[j]) ):
                cont1 = cont1 + 1
                
    if tamanioUnion == 0:
        return 0.0
    else:
        return (cont1/tamanioUnion)*100


def similitudSustantivosPorcentaje1(texto1, texto2):
    pool1 = ThreadPool(processes=1)
    pool2 = ThreadPool(processes=1)
    
    async_result1 = pool1.apply_async(sustantivosOraciones, (texto1,))
    async_result2 = pool2.apply_async(sustantivosOraciones, (texto2,))
    
    sustantivos1 = async_result1.get()
    sustantivos2 = async_result2.get()

    pool3 = ThreadPool(processes=1)
    pool4 = ThreadPool(processes=1)
    
    async_result3 = pool3.apply_async(convierteInfinitivoLista, (sustantivos1,))
    async_result4 = pool4.apply_async(convierteInfinitivoLista, (sustantivos2,))
    
    Sustantivos1 = async_result3.get()
    Sustantivos2 = async_result4.get()
    
    

    tamanioUnion = len(Sustantivos1) + len(Sustantivos2)
    cont1 = 0

    for i in range(0, len(Sustantivos1)):
        for j in range(0, len(Sustantivos2)):
            #print('se compara {0} con {1}'.format(Sustantivos1[i], Sustantivos2[j]))
            #print(identificaSinonimo(Sustantivos1[i], Sustantivos2[j]))
            if( identificaSinonimo(Sustantivos1[i], Sustantivos2[j]) ):
                cont1 = cont1 + 1
                
    if tamanioUnion == 0:
        return 0.0
    else:
        return (cont1/tamanioUnion)*100


    
def similitudAdverbiosPorcentaje1(texto1, texto2):
    pool1 = ThreadPool(processes=1)
    pool2 = ThreadPool(processes=1)
    
    async_result1 = pool1.apply_async(adverbioOraciones, (texto1,))
    async_result2 = pool2.apply_async(adverbioOraciones, (texto2,))
    
    adverbios1 = async_result1.get()
    adverbios2 = async_result2.get()
    
    pool3 = ThreadPool(processes=1)
    pool4 = ThreadPool(processes=1)
    
    async_result3 = pool3.apply_async(convierteInfinitivoLista, (adverbios1,))
    async_result4 = pool4.apply_async(convierteInfinitivoLista, (adverbios2,))

    Adverbios1 = convierteInfinitivoLista(adverbios1)
    Adverbios2 = convierteInfinitivoLista(adverbios2)


    tamanioUnion = len(Adverbios1) + len(Adverbios2)
    cont1 = 0

    for i in range(0, len(Adverbios1)):
        for j in range(0, len(Adverbios2)):
            #print('se compara {0} con {1}'.format(Adverbios1[i], Adverbios2[j]))
            #print(identificaSinonimo(Adverbios1[i], Adverbios2[j]))
            if( identificaSinonimo(Adverbios1[i], Adverbios2[j]) ):
                cont1 = cont1 + 1
                
    if tamanioUnion == 0:
        return 0.0
    else:
        return (cont1/tamanioUnion)*100   
    
    
    
def similitudAdjetivosPorcentaje1(texto1, texto2):
    pool1 = ThreadPool(processes=1)
    pool2 = ThreadPool(processes=1)
    
    async_result1 = pool1.apply_async(adjetivosOraciones, (texto1,))
    async_result2 = pool2.apply_async(adjetivosOraciones, (texto2,))
    
    adjetivos1 = async_result1.get()
    adjetivos2 = async_result1.get()
    
    pool3 = ThreadPool(processes=1)
    pool4 = ThreadPool(processes=1)
    
    async_result3 = pool3.apply_async(convierteInfinitivoLista, (adjetivos1,))
    async_result4 = pool4.apply_async(convierteInfinitivoLista, (adjetivos2,))

    Adjetivos1 = convierteInfinitivoLista(adjetivos1)
    Adjetivos2 = convierteInfinitivoLista(adjetivos2)


    tamanioUnion = len(Adjetivos1) + len(Adjetivos2)
    cont1 = 0

    for i in range(0, len(Adjetivos1)):
        for j in range(0, len(Adjetivos2)):
            #print('se compara {0} con {1}'.format(Adjetivos1[i], Adjetivos2[j]))
            #print(identificaSinonimo(Adjetivos1[i], Adjetivos2[j]))
            if( identificaSinonimo(Adjetivos1[i], Adjetivos2[j]) ):
                cont1 = cont1 + 1
                
    if tamanioUnion == 0:
        return 0.0
    else:
        return (cont1/tamanioUnion)*100   
    
    
    
    
def similitudOtrosPorcentaje1(texto1, texto2):
    pool1 = ThreadPool(processes=1)
    pool2 = ThreadPool(processes=1)
    
    async_result1 = pool1.apply_async(otrosOraciones, (texto1,))
    async_result2 = pool2.apply_async(otrosOraciones, (texto2,))
    
    otros1 = async_result1.get()
    otros2 = async_result2.get()
    
    pool3 = ThreadPool(processes=1)
    pool4 = ThreadPool(processes=1)
    
    async_result3 = pool3.apply_async(convierteInfinitivoLista, (otros1,))
    async_result4 = pool4.apply_async(convierteInfinitivoLista, (otros2,))
    
    Otros1 = convierteInfinitivoLista(otros1)
    Otros2 = convierteInfinitivoLista(otros2)


    tamanioUnion = len(Otros1) + len(Otros2)
    cont1 = 0

    for i in range(0, len(Otros1)):
        for j in range(0, len(Otros2)):
            #print('se compara {0} con {1}'.format(Otros1[i], Otros2[j]))
            #print(identificaSinonimo(Otros1[i], Otros2[j]))
            if( identificaSinonimo(Otros1[i], Otros2[j]) ):
                cont1 = cont1 + 1
    
    if tamanioUnion == 0:
        return 0.0
    else:
        return (cont1/tamanioUnion)*100    
    
def porcentajeParecidoTextos1(texto1, texto2):
    pool1 = ThreadPool(processes=1)
    pool2 = ThreadPool(processes=1)
    pool3 = ThreadPool(processes=1)
    pool4 = ThreadPool(processes=1)
    pool5 = ThreadPool(processes=1)
    async_result1 = pool1.apply_async(similitudVerbosPorcentaje1, (texto1,texto2))
    async_result2 = pool2.apply_async(similitudSustantivosPorcentaje1, (texto1,texto2))
    async_result3 = pool3.apply_async(similitudAdjetivosPorcentaje1, (texto1,texto2))
    async_result4 = pool4.apply_async(similitudAdverbiosPorcentaje1, (texto1,texto2))
    async_result5 = pool5.apply_async(similitudOtrosPorcentaje1, (texto1,texto2))
    
    verbo = async_result1.get()
    sustantivo = async_result2.get()
    adjetivo = async_result3.get()
    adverbio = async_result4.get()
    otro = async_result5.get()
    
    return (40*verbo + 40*sustantivo + 8*adjetivo + 7*adverbio + 5*otro)/100    
    
    
    
def parecidoEntreTextos1(texto1, texto2):
    parecido = porcentajeParecidoTextos1(texto1, texto2)
    if( parecido < 10 ):
        return 0
    elif( 10 <= parecido and parecido < 16 ):
        return 1
    elif( 16 <= parecido and parecido <= 25 ):
        return 2
    else:
        return 3    

# funcion para ver el parecido con los otros
# textos 
def PorcentajeParecido(texto1):
    # obtener todos los textos
    lista = Textos.query.all()
    # en desarrollo
    # solo usa un texto para prueba
    texto2 = lista[3].text
    pj = porcentajeParecidoTextos1(texto1, texto2)
    print(pj)

    return pj


query = "file"
reply = " "
puntaje = " "

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = dbdir
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)

class Textos(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    text = db.Column(db.Text)

@app.route('/inicio')
def inicio():
    return redirect('/')

@app.route('/')
def hello_world():
    return render_template('index.html', file=query, texto=reply, puntaje=puntaje)

@app.route('/signup', methods = ['POST'])
def signup():
    global query
    global reply
    query = request.form['avatar']
    name = request.form['name']
    response = 'Probando texto insertado' #ExtraerTexto(query)
    #  insertar el texo en una BD
    new_post = Textos(title=name, text = response)
    db.session.add(new_post)
    db.session.commit()
    #print(response)
    reply = response
    return redirect('/')

@app.route('/ranking')
def ranking():
    # ff
    return render_template('ranking.html',textos=Textos.query.all() ,valor1=70, mostrar='true')

@app.route('/ver')
def ConsultaRanking():
    # ff
    texto1 = 'Manuel esta jugando fútbol en el parque con sus amigos.'
    valor = PorcentajeParecido(texto1)
    return render_template('ranking.html',textos=Textos.query.all() ,valor1=valor,mostrar='false')


    return PorcentajeParecido(texto1)

if __name__ == "__main__":
    db.create_all()
    app.run(debug=True)


